#!/usr/bin/env ruby

require 'dotenv'
require 'httparty'
require 'nokogiri'
require 'rspotify'
require 'uri'

script_path  = File.symlink?(__FILE__) ? File.readlink(__FILE__) : __FILE__
env_filepath = File.expand_path("../.env", script_path)
Dotenv.load(env_filepath)

def headers_with(token)
    {
        "Accept" => "application/json",
        "Content-Type" => "application/json",
        "Authorization" => "Bearer #{token}"
    }
end

def get_song_details
    headers = headers_with ENV["SPOTIFY_TOKEN"]
    response = HTTParty.get("https://api.spotify.com/v1/me/player/currently-playing", headers: headers)

    name = response["item"]["name"]
    artist = response["item"]["artists"][0]["name"]

    query_text = "#{name} #{artist}"
end

def get_lyrics(query)
    headers = headers_with ENV["GENIUS_TOKEN"]
    response = HTTParty.get("http://api.genius.com/search?q=#{query}", headers: headers)

    hits = response["response"]["hits"]

    if hits.empty?
        return nil

    else
        url = hits[0]["result"]["url"]
        response = HTTParty.get(url)
        doc = Nokogiri::HTML(response.body)

        song_name = "* * * " + doc.css('.song_header-primary_info-title').text.upcase + " * * *"
        lyrics = doc.css('.lyrics p').text.gsub(/\[.*\]/, "").gsub(/^$\n^$\n/, "\n").strip!

        lyrics
    end
end

query_text = get_song_details

query  = URI::encode(query_text)
lyrics = get_lyrics(query)

unless lyrics.nil?
    puts lyrics

else
    puts "No songs found matching '#{query_text}'"
end
