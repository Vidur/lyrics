
This is a quick and dirty script to get the currently playing spotify song's lyrics right in the terminal.

## Usage

0. Run `bundle` to install all the gem dependencies
1. Create your `.env` file with your Spotify and Genius tokens
2. Copy the `.env.sample` file to a `.env` file
3. Get your Spotify OAuth token (requires a Spotify user account)
    1. Go to the Spotify Web API console, e.g.: https://developer.spotify.com/console/get-users-currently-playing-track/
    2. Generate an OAuth token, and copy/paste
4. Get your Genius OAuth token (requires a Genius user account)
    1. Go to the Genius API console, e.g.: https://docs.genius.com/#!#account-h2
    2. Click on "Authorize With Genius", and go through the steps to generate a token
5. You should be able to execute `./run`, while you're listening to a Spotify song on your phone or wherever, and have the lyrics show up in your terminal

## Symlink

I keep a symlink to this tool in my PATH by symlinking it in to my `~/bin` directory:

```
❯ cd ~/bin
❯ ln -sn /path/to/spotify-lyrics/run lyrics
```

So now you should be able to exec `lyrics` from any path in your terminal.

## TODO

- [] Turn this into a local web app to solve:
    - [] Spotify Web API Token expires after 15mins(?)
    - [] Page reloads to new song automatically
    - [] Easier to read the lyrics
- [] Perhaps switch to AZ Lyrics as the primary lyrics provider, and fallback to Genius
- [] Reduce the gem dependencies
- [] Better error messages lol